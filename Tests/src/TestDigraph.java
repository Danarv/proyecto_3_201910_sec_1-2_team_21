package src;

import junit.framework.TestCase;
import model.data_structures.ArcoTest;
import model.data_structures.DiGraph;
import model.data_structures.VerticeTest;
import model.exceptions.ArcoNoExisteException;
import model.exceptions.ArcoYaExisteException;
import model.exceptions.VerticeNoExisteException;
import model.exceptions.VerticeYaExisteException;

public class TestDigraph extends TestCase{
	
	//Atributos
	
	/**
	 * Grafo sobre el que se van a hacer las pruebas.
	 */
	private DiGraph<Integer, VerticeTest, ArcoTest> grafo;
	
	//Escenarios
	
	/**
	 * Grafo vac�o
	 */
	public void setUpEscenario1() {
		grafo = new DiGraph<Integer, VerticeTest, ArcoTest>();
	}
	
	/**
	 * Grafo con 10 vertices y 0 arcos
	 */
	public void setUpEscenario2() {
		grafo = new DiGraph<Integer, VerticeTest, ArcoTest>();
		
		for(int i = 1; i < 11; i++) {
			VerticeTest x = new VerticeTest(100 + i, i);
			try {
				grafo.addVertex(x.getId(), x);
			} 
			catch (VerticeYaExisteException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Grafo con 100 vertices y 0 arcos
	 */
	public void setUpEscenario3() {
		grafo = new DiGraph<Integer, VerticeTest, ArcoTest>();
		
		for(int i = 1; i < 101; i++) {
			VerticeTest x = new VerticeTest(1000 + i, i);
			try {
				grafo.addVertex(x.getId(), x);
			} 
			catch (VerticeYaExisteException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Grafo con 9 vertices y 10 arcos
	 */
	public void setUpEscenario4() {
		grafo = new DiGraph<Integer, VerticeTest, ArcoTest>();
		
		for(int i = 1; i < 10; i++) {
			VerticeTest x = new VerticeTest(100 + i, i);
			try {
				grafo.addVertex(x.getId(), x);
			} 
			catch (VerticeYaExisteException e) {
				e.printStackTrace();
			}
		}
		
		ArcoTest a = null;
		try {
			a = new ArcoTest( 1 );
			grafo.addEdge(101, 109, a );
			a = new ArcoTest( 2 );
			grafo.addEdge(105, 101, a );
			a = new ArcoTest( 3 );
			grafo.addEdge(106, 101, a );
			a = new ArcoTest( 4 );
			grafo.addEdge(101, 107, a );
			a = new ArcoTest( 5 );
			grafo.addEdge(101, 102, a );
			a = new ArcoTest( 6 );
			grafo.addEdge(104, 109, a );
			a = new ArcoTest( 7 );
			grafo.addEdge(106, 107, a );
			a = new ArcoTest( 8 );
			grafo.addEdge(101, 108, a );
			a = new ArcoTest( 9 );
			grafo.addEdge(101, 103, a );
			a = new ArcoTest( 10 );
			grafo.addEdge(102, 107, a );
		}
		catch (VerticeNoExisteException|ArcoYaExisteException e) {
			e.printStackTrace();
		}
	}
	
	//Pruebas
	
	public void testAdd() {
		setUpEscenario1();
		VerticeTest nuevo = new VerticeTest(101, 1);
		VerticeTest nuevo2 = new VerticeTest(102, 2);
		//Add vertice
		try {
			grafo.addVertex(nuevo.getId(), nuevo);
			grafo.addVertex(nuevo2.getId(), nuevo2);
			assertEquals(2, grafo.getNVertex());
		}
		catch(VerticeYaExisteException e) {
			e.printStackTrace();
		}
		
		ArcoTest nuevoArc = new ArcoTest(10);
		
		//Add arco
		try {
			grafo.addEdge(nuevo.getId(), nuevo2.getId(), nuevoArc);
			assertEquals(1, grafo.getNEdges());
		}
		catch(VerticeNoExisteException|ArcoYaExisteException e) {
			e.printStackTrace();
		}
	}
	
	public void testDelete() {
		//Delete vertice
		setUpEscenario2();
		try {
			grafo.deleteVertex(102);
		}
		catch (VerticeNoExisteException e) {
			e.printStackTrace();
		}
		assertEquals(9, grafo.getNVertex());
		
		//Delete arco
		setUpEscenario4();
		try {
			grafo.deleteEdge(101, 103);
		} 
		catch (VerticeNoExisteException | ArcoNoExisteException e) {
			e.printStackTrace();
		}
		assertEquals(9, grafo.getNEdges());
	}
	
	public void testGetInfo() {
		setUpEscenario4();
		//Info vertice
		assertEquals(5, grafo.getInfoVertex(105).getInfo().intValue());
		//Info arco
		assertEquals(1, grafo.getInfoArc(101, 109).darInfo());
	}
	
	public void testSetInfo() {
		setUpEscenario4();
		//Set info vertice
		VerticeTest i = new VerticeTest(101, 20);
		grafo.setInfoVertex(101, i);
		assertEquals(20, grafo.getInfoVertex(101));
		//Set info arco
		ArcoTest a = new ArcoTest(20);
		grafo.setInfoArc(102, 107, a);
		assertEquals(20, grafo.getInfoArc(102, 107));
	}
}
