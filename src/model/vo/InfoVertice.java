package model.vo;

import java.util.ArrayList;

public class InfoVertice {
	
	//Atributos 
	
	/**
	 * id del vertice
	 */
	private int id;
	
	/**
	 * latitud del vertice
	 */
	private double lat;
	
	/**
	 * longitud del vertice
	 */
	private double lon;
	
	/**
	 * Violaciones de transito cerca al vertice
	 */
	private ArrayList<VOMovingViolations> infoVertice;
	
	public InfoVertice(int pId, double pLat, double pLon) {
		id = pId;
		lat = pLat;
		lon = pLon;
		infoVertice = new ArrayList<VOMovingViolations>();
	}
	
	public int getId() {
		return id;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLon() {
		return lon;
	}
	
	public ArrayList<VOMovingViolations> getInfoVertex() {
		return infoVertice;
	}
}
