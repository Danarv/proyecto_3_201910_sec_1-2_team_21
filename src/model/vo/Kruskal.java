package model.vo;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Kruskal {

	private static final double FLOATING_POINT_EPSILON = 1E-12;
	
	static class Vertice{
		int origen;
		int destino;
		int peso; 
		
		public Vertice(int origen, int destino, int peso){
			this.origen = origen;
			this.destino = destino;
			this.peso = peso;			
		}
	}
	
	static class Grafo{
		int vertices;
		ArrayList<Vertice> todos = new ArrayList<>();
		
		Grafo(int vertices){
			this.vertices = vertices;
		}
		
		public void agregarVertice(int origen, int destino, int peso){
			Vertice v = new Vertice(origen, destino, peso);
			todos.add(v);
		}
		
		public void kruskalMST(){
			PriorityQueue<Vertice> pq = new PriorityQueue<>(todos.size(), Comparator.comparingInt(o -> o.peso));
			
			for(int i = 0; i<todos.size(); i++){
				pq.add(todos.get(i));
			}
			
			int[] padre = new int[vertices];
			
			hacerConjunto(padre);
			
			ArrayList<Vertice> mst = new ArrayList<>();
			
			int ind = 0;
			
			while(ind < vertices-1){
				Vertice v = pq.remove();
				int x_set = buscar(padre, v.origen);
				int y_set = buscar(padre, v.destino);
				
				if(x_set == y_set){						}
				else{	
					mst.add(v);
					ind++;
					union(padre, x_set, y_set);
				}
			}
			
			System.out.println("Minimum spanning tree:");
			dibujoGrafo(mst);
		}
		
		public void hacerConjunto(int[] padre){
			for(int i = 0; i < vertices; i++)
				padre[i] = i;
		}
		
		public int buscar(int[] padre, int vertice){
			if(padre[vertice] != vertice){ return buscar(padre, padre[vertice]);}
			return vertice;
		}
		
		public void union(int[] padre, int x, int y){
			int x_darPadre = buscar(padre, x);
			int y_darPadre = buscar(padre, y);
			
			padre[y_darPadre] = x_darPadre;
	}
		
		public void dibujoGrafo(ArrayList<Vertice> listaVertices){
			for(int i = 0; i <listaVertices.size(); i++){
				Vertice v = listaVertices.get(i);
				System.out.println("Vertice- " + i + "Fuente: " + v.origen + "Destino: " + v.destino + "Peso: " + v.peso); 
			}
		}
	
	
}
}
