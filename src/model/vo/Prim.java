package model.vo;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import java.util.*;

public class Prim {
	
	public static int AlgoritmoDePrim( Vertice[] grafo, int fuente){
		int cost = 0;
		boolean[] visitados = new boolean[grafo.length];
		PriorityQueue<Edge> pq = new PriorityQueue<Edge>();
		
		visitados[fuente] = true;
		
		for(Edge e: grafo[fuente].vecinos){
			pq.add(e);
		}
		
		while(!pq.isEmpty()){
			Edge e = pq.remove();
			
			if(visitados[e.end]){	continue;}
			
			visitados[e.end] = true;
			
			cost += e.cost;
			
			for(Edge veci: grafo[e.end].vecinos){	pq.add(veci);}
			
			for(int i = 1; i<grafo.length; i++){ 	if(!visitados[i]) return -1; }
		}
		
		
		return cost;
		
	}
	
	
	static class Vertice{
		List<Edge> vecinos;
		
		public Vertice(){
			vecinos = new ArrayList<Edge>();
		}
		
	}
	
	static class Edge implements Comparable<Edge>{
		
		int end, cost;
		
		public Edge (int end, int cost){
			this.end = end;
			this.cost = cost;
		}
		
		public int compareTo (Edge e){
			return this.cost - e.cost;
		}
	}

}
