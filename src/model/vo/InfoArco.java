package model.vo;

import model.data_structures.IArco;

public class InfoArco implements IArco 
{
	
	private double peso;
	
	private Double distancia;
	
	private int tiempo;
	
	private Double dinero;
	
	private int peaje;
	
	private int contador;
	
	
	public InfoArco( Double pDistancia, int pTiempo, Double pDinero, int pPeaje, int pContador) {
		// TODO Auto-generated constructor stub
		
		distancia = pDistancia;
		tiempo = pTiempo;
		dinero = pDinero;
		peaje = pPeaje;
		contador = pContador;
	}
	
	public void actualizarInfoArco(double pDistancia, int pTiempo, double pDinero, int pPeaje)
	{
		contador++;
		distancia += pDistancia;
		tiempo += pTiempo;
		dinero += pDinero;
		peaje += pPeaje;
	}
	
	@Override
	public double darPeso() {
		// TODO Auto-generated method stub
		peso = (distancia + tiempo + dinero + peaje)/contador;
		return peso ;
	}

	
	public double getPeso() {
		return peso;
	}

	
	public void setPeso(int peso) {
		this.peso = peso;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}


	public int getTiempo() {
		return tiempo/contador;
	}


	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}
	

	public double getDinero() {
		return dinero/contador;
	}


	public void setDinero(double dinero) {
		this.dinero = dinero;
	}


	public int getPeaje() {
		return peaje;
	}


	public void setPeaje(int peaje) {
		this.peaje = peaje;
	}
	
	public int darContador() {
		return contador;
	}

}
