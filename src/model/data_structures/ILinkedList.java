package model.data_structures;

public interface ILinkedList<A extends IArco, V> {
	
	/**
	 * Devuelve true si la lista est� vacia, False de lo contrario
	 */
	public boolean isEmpty();
	
	/**
	 * Inserta un elemento en la lista
	 * @param nElement. Elemento a agregar.
	 */
	public void add(A nElement);
	
	/**
	 * Inserta un nuevo elemento en la cabeza de la lista.
	 * @param nElement. Elemento a agregar.
	 */
	public void delete(A nElement) throws Exception;
	
	
}
