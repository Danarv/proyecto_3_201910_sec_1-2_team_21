package model.data_structures;

import java.util.Iterator;

public class List<V> implements Iterable<V>{
	
	private class NodoLista<V>{
		V item;
		NodoLista<V> siguiente;
		NodoLista<V> anterior;
		
		public NodoLista(V pItem) {
			item = pItem;
			siguiente = null;
			anterior = null;
		}
		
		public NodoLista<V> getSiguiente() {
			return siguiente;
		}
		
		public void setSiguiente(NodoLista<V> pNuevoNxt) {
			this.siguiente = pNuevoNxt;
		}
		
		public V getItem() {
			return item;
		}
		
		public void cambiarItem(V pNewItem) {
			this.item = pNewItem;
		}
		
		public NodoLista<V> getAnterior() {
			return anterior;
		}
		
		public void setAnterior(NodoLista<V> pNuevoAnt) {
			this.siguiente = pNuevoAnt;
		}
		
		public void desconectarNodo( ) {
			NodoLista<V> ant = anterior;
			NodoLista<V> sig = siguiente;
			anterior = null;
			siguiente = null;
			ant.siguiente = sig;
			
			if( sig != null ) {
				sig.anterior = ant;
				}
		}
	}
	
	//Atriibutos
	
	private int size;
	
	private NodoLista<V> first;
	
	private NodoLista<V> last;
	
	//Constructor
	
	public List() {
		this.size = 0;
		this.first = new NodoLista<V>(null);
		this.last = new NodoLista<V>(null);
		first.setSiguiente(last);
		last.setAnterior(first);
	}
	
	//M�todos
	
	public int getSize() {
		return size;
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public NodoLista<V> getFirst() {
		return first;
	}
	
	public NodoLista<V> getLast() {
		return last;
	}
	
	public V buscar(V pItem) {
		for(NodoLista<V> x = first; x != null; x = x.getSiguiente()) {
			if(x.getItem().equals(pItem)) {
				return x.getItem();
			}
		}
		return null;
	}
	
	public void addBetween(V pItem, NodoLista<V> pAnterior, NodoLista<V> pSiguiente) {
		NodoLista<V> nuevo = new NodoLista<V>(pItem);
		pAnterior.setSiguiente(nuevo);
		pSiguiente.setAnterior(nuevo);
		size++;
	}
	
	public void addFirst(V item) {
		addBetween(item , first, first.getSiguiente());
	}
	
	public void addLast(V item) {
		addBetween(item , last.getAnterior(), last);
	}
	
	public void add(V item) {
		if(isEmpty()) {
			addFirst(item);
		}
		else {
			addLast(item);
		}
	}
	
	public void delete(V pItem){
		NodoLista<V> aEliminar = first;
		NodoLista<V> anteriorEliminar = null;
		if(buscar(pItem) != null) {
			boolean ya = false;
			while(ya) {
				if(aEliminar.getItem() != pItem) {
					aEliminar = aEliminar.getSiguiente();
				}
				else {
					ya = true;
				}
			}
			anteriorEliminar = aEliminar.getAnterior();
			aEliminar.setAnterior(null);
			anteriorEliminar.setSiguiente(null);
			
			if(last.getItem().equals(pItem)){
				last = anteriorEliminar;
			}
		}
	}
	
	@Override
	public Iterator<V> iterator() {
		Queue<V> queue = new Queue<V>();
        for (NodoLista<V> x = first; x != null; x = x.getSiguiente())
            queue.enqueue(x.item);
        return queue.iterator();
	}
}
