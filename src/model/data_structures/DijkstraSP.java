package model.data_structures;

import java.util.HashMap;

import model.vo.InfoArco;
import model.vo.InfoVertice;
import model.vo.LonLat;

/******************************************************************************
 *  Compilation:  javac DijkstraSP.java
 *  Execution:    java DijkstraSP input.txt s
 *  Dependencies: EdgeWeightedDigraph.java IndexMinPQ.java Stack.java DirectedEdge.java
 *  Data files:   https://algs4.cs.princeton.edu/44sp/tinyEWD.txt
 *                https://algs4.cs.princeton.edu/44sp/mediumEWD.txt
 *                https://algs4.cs.princeton.edu/44sp/largeEWD.txt
 *
 *  Dijkstra's algorithm. Computes the shortest path tree.
 *  Assumes all weights are nonnegative.
 *
 *  % java DijkstraSP tinyEWD.txt 0
 *  0 to 0 (0.00)  
 *  0 to 1 (1.05)  0->4  0.38   4->5  0.35   5->1  0.32   
 *  0 to 2 (0.26)  0->2  0.26   
 *  0 to 3 (0.99)  0->2  0.26   2->7  0.34   7->3  0.39   
 *  0 to 4 (0.38)  0->4  0.38   
 *  0 to 5 (0.73)  0->4  0.38   4->5  0.35   
 *  0 to 6 (1.51)  0->2  0.26   2->7  0.34   7->3  0.39   3->6  0.52   
 *  0 to 7 (0.60)  0->2  0.26   2->7  0.34   
 *
 *  % java DijkstraSP mediumEWD.txt 0
 *  0 to 0 (0.00)  
 *  0 to 1 (0.71)  0->44  0.06   44->93  0.07   ...  107->1  0.07   
 *  0 to 2 (0.65)  0->44  0.06   44->231  0.10  ...  42->2  0.11   
 *  0 to 3 (0.46)  0->97  0.08   97->248  0.09  ...  45->3  0.12   
 *  0 to 4 (0.42)  0->44  0.06   44->93  0.07   ...  77->4  0.11   
 *  ...
 *
 ******************************************************************************/


/**
 *  The {@code DijkstraSP} class represents a data type for solving the
 *  single-source shortest paths problem in edge-weighted digraphs
 *  where the edge weights are nonnegative.
 *  <p>
 *  This implementation uses Dijkstra's algorithm with a binary heap.
 *  The constructor takes time proportional to <em>E</em> log <em>V</em>,
 *  where <em>V</em> is the number of vertices and <em>E</em> is the number of edges.
 *  Each call to {@code distTo(int)} and {@code hasPathTo(int)} takes constant time;
 *  each call to {@code pathTo(int)} takes time proportional to the number of
 *  edges in the shortest path returned.
 *  <p>
 *  For additional documentation,    
 *  see <a href="https://algs4.cs.princeton.edu/44sp">Section 4.4</a> of    
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne. 
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 *  Modificado por David Saavedra y Diany Quintero
 */
public class DijkstraSP 
{

	// ------------------------------------------------------------------
	//	CONSTANTES
	// ------------------------------------------------------------------

	public final static int DISTANCIA = 1;

	public final static int MENOR_TIEMPO = 2;


	private HashMap<LonLat, Double> disTo; // Llave y su valor asociado la distancia
	private double[] distTo;          // distTo[v] = distance  of shortest s->v path
	private HashMap<LonLat, Arco<LonLat, InfoVertice<LonLat>, InfoArco>> edgeto;
	private Arco[] edgeTo;    // edgeTo[v] = last edge on shortest s->v path
	private IndexMinPQ<Double> pq;    // priority queue of vertices
	private GrafoDirigido<LonLat, InfoVertice<LonLat>, InfoArco> g;
	private int constante;
	
	/**
	 * Computes a shortest-paths tree from the source vertex {@code s} to every other
	 * vertex in the edge-weighted digraph {@code G}.
	 *
	 * @param  G the edge-weighted digraph
	 * @param  s the source vertex
	 * @throws IllegalArgumentException if an edge weight is negative
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public DijkstraSP(GrafoDirigido<LonLat, InfoVertice<LonLat>, InfoArco> G, LonLat s, int c) {

		g = G;
		constante = c;
		for (int i = 0; i < g.darNArcos(); i++)
		{	
			double arcos =  g.darArcos().dar(i).getDistancia();
			if (arcos < 0)
				throw new IllegalArgumentException("edge " + arcos + " has negative weight");
		}

		disTo = new HashMap<LonLat, Double>( );
		edgeto = new HashMap<LonLat,Arco<LonLat, InfoVertice<LonLat>, InfoArco>>();

		validateVertex(s);
		for (Vertice <LonLat, InfoVertice<LonLat>, InfoArco>  v: g.darRecorridoPlano()) 
		{
		
				disTo.put(v.darId(), Double.POSITIVE_INFINITY );
		}
		disTo.put(s, 0.0);

		// relax vertices in order of distance from s
		pq = new IndexMinPQ<Double>(g.darNVertices()+1);

		int s1;
		try {
			s1 = g.darListaVertice().indexOf(g.darObjetoVertice(s));
			pq.insert(s1, disTo.get(s));

			while (!pq.isEmpty()) 
			{
				//				System.out.println("Volvi");
				int v = pq.delMin();//System.out.println(v +" Soy el menor que sale de la cola");
				//				System.out.println(g.darListaVertice().dar(v) + " asdasd");
				boolean variable = g.darListaVertice().dar(v).darNumeroSucesores() >0;

				if( !variable  );
				{
					//					System.out.println("Entre 112");
					//					System.out.println(g.darListaVertice().dar(v).darNumeroSucesores());
					for (int i = 0; i< g.darListaVertice().dar(v).darNumeroSucesores(); i ++ )
					{
						//						System.out.println( v + "Tengo sucesores");
						//						System.out.println( " Aqui fallo "+ g.darListaVertice().dar(v).darSucesores().get(i) +" Sali del fallo");
						Arco e = g.darListaVertice().dar(v).darSucesores().get(i);
						//						System.out.println( v + "Tengo sucesores");
						relax(e ,G);
					}
					//					System.out.println(" Sali del for ");
				}
				//				System.out.println(" Sali del for ");
				//				System.out.println(pq.isEmpty());
				//				System.out.println("Siguiete");
			}
			//			System.out.println("sali del while");
		} catch (VerticeNoExisteException e1) {

			e1.printStackTrace();
		} 




		// check optimality conditions
		assert check(G, s);
	}

	// relax edge e and update pq if changed
	private void relax( Arco<LonLat, InfoVertice<LonLat>, InfoArco> e, GrafoDirigido <LonLat, InfoVertice<LonLat>, InfoArco> g) {
		LonLat v = e.darVerticeOrigen().darId(), w = e.darVerticeDestino().darId();
		int w1 = g.darListaVertice().indexOf(e.darVerticeDestino());

		switch(constante)
		{
		case 1:
			
			if (disTo.get(w) > disTo.get(v) + e.darInfoArco().getDistancia() )
			{
				disTo.put(w,  disTo.get(v) + e.darInfoArco().getDistancia());

				edgeto.put(w, e);

				if( pq.contains(w1))
				{
					pq.decreaseKey(w1, disTo.get(w));
				}
				else
				{
					pq.insert(w1, disTo.get(w));
				}
			}
			break;
			
		case 2:
			
			if (disTo.get(w) > disTo.get(v) + e.darInfoArco().getTiempo() )
			{
				disTo.put(w,  disTo.get(v) + e.darInfoArco().getTiempo());

				edgeto.put(w, e);

				if( pq.contains(w1))
				{
					pq.decreaseKey(w1, disTo.get(w));
				}
				else
				{
					pq.insert(w1, disTo.get(w));
				}
			}
			
			break;			
		}
		
	}

	/**
	 * Returns the length of a shortest path from the source vertex {@code s} to vertex {@code v}.
	 * @param  v the destination vertex
	 * @return the length of a shortest path from the source vertex {@code s} to vertex {@code v};
	 *         {@code Double.POSITIVE_INFINITY} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public double distTo( LonLat v , GrafoDirigido  g) {
		validateVertex(v);
		return disTo.get(v);
	}

	/**
	 * Returns true if there is a path from the source vertex {@code s} to vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return {@code true} if there is a path from the source vertex
	 *         {@code s} to vertex {@code v}; {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(LonLat v) {
		validateVertex(v);
		return disTo.get(v) < Double.POSITIVE_INFINITY;
	}

	/**
	 * Returns a shortest path from the source vertex {@code s} to vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return a shortest path from the source vertex {@code s} to vertex {@code v}
	 *         as an iterable of edges, and {@code null} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Stack<Arco <LonLat, InfoVertice<LonLat>, InfoArco>> pathTo(LonLat v)
	{

		validateVertex(v);

		if (!hasPathTo(v)) return null;

		Stack<Arco<LonLat, InfoVertice<LonLat>, InfoArco>> path = new Stack<Arco<LonLat, InfoVertice<LonLat>, InfoArco>>();

		for (Arco <LonLat, InfoVertice<LonLat>, InfoArco> e = edgeto.get(v); e != null; e = edgeto.get(e.darVerticeOrigen().darId())) {

			path.push(e);
		}
		return path;
	}


	// check optimality conditions:
	// (i) for all edges e:            distTo[e.to()] <= distTo[e.from()] + e.weight()
	// (ii) for all edge e on the SPT: distTo[e.to()] == distTo[e.from()] + e.weight()
	private boolean check(GrafoDirigido <LonLat, InfoVertice<LonLat>, InfoArco> G, LonLat s) {

		for (int i = 0; i < G.darNArcos(); i++)
		{	
			double arcos =  G.darArcos().dar(i).getDistancia();
			if (arcos < 0)
			{
				System.err.println("negative edge weight detected");
				return false;
			}
		}
		//		// check that edge weights are nonnegative
		//		for (DirectedEdge e : G.edges()) {
		//			if (e.weight() < 0) {
		//				System.err.println("negative edge weight detected");
		//				return false;
		//			}
		//		}

		if ( disTo.get(s) != 0.0 || edgeto.get(s) != null )
		{
			System.err.println("distTo[s] and edgeTo[s] inconsistent");
			return false;
		}
		//		// check that distTo[v] and edgeTo[v] are consistent
		//		if (distTo[s] != 0.0 || edgeTo[s] != null) {
		//			System.err.println("distTo[s] and edgeTo[s] inconsistent");
		//			return false;
		//		}
		for (Vertice <LonLat, InfoVertice<LonLat>, InfoArco>  v: G.darRecorridoPlano()) 
		{
			if( v.darId().equals(s) )
			{

			}
			else if( edgeto.get(v.darId()) == null && disTo.get(v.darId())!= Double.POSITIVE_INFINITY)
			{
				System.err.println("distTo[] and edgeTo[] inconsistent");
				return false;
			}
		}
		//		for (int v = 0; v < G.V(); v++) {
		//			if (v == s) continue;
		//			if (edgeTo[v] == null && distTo[v] != Double.POSITIVE_INFINITY) {
		//				System.err.println("distTo[] and edgeTo[] inconsistent");
		//				return false;
		//			}
		//		}

		for (Vertice <LonLat, InfoVertice<LonLat>, InfoArco>  v: G.darRecorridoPlano()) 
		{
			for (Arco <LonLat, InfoVertice<LonLat>, InfoArco> e : v.darSucesores())
			{
				LonLat w1 = e.darVerticeDestino().darId();
				if( disTo.get(v.darId()) + e.darInfoArco().getDistancia() < disTo.get(w1))
				{
					System.err.println("edge " + e + " not relaxed");
					return false;
				}
			}
		}

		// check that all edges e = v->w satisfy distTo[w] <= distTo[v] + e.weight()
		//		for (int v = 0; v < G.V(); v++) {
		//			for (DirectedEdge e : G.adj(v)) {
		//				
		//				int w = e.to();
		//				if (distTo[v] + e.weight() < distTo[w]) {
		//					System.err.println("edge " + e + " not relaxed");
		//					return false;
		//				}
		//			}
		//		}
		for (Vertice <LonLat, InfoVertice<LonLat>, InfoArco>  w: G.darRecorridoPlano()) 
		{
			if( edgeto.get(w.darId()) == null)
			{

			}
			else
			{
				Arco<LonLat, InfoVertice<LonLat>,InfoArco> e = edgeto.get(w.darId());
				Vertice <LonLat, InfoVertice<LonLat>, InfoArco> v = e.darVerticeOrigen();
				if( !(w.equals(v)) ) return false;
				if( disTo.get(v.darId()) + e.darInfoArco().getDistancia() != disTo.get(w.darId()) )
				{
					System.err.println("edge " + e + " on shortest path not tight");
					return false;
				}
			}
		}
		//		// check that all edges e = v->w on SPT satisfy distTo[w] == distTo[v] + e.weight()
		//		for (int w = 0; w < G.V(); w++) {
		//			if (edgeTo[w] == null) continue;
		//			DirectedEdge e = edgeTo[w];
		//			int v = e.from();
		//			if (w != e.to()) return false;
		//			if (distTo[v] + e.weight() != distTo[w]) {
		//				System.err.println("edge " + e + " on shortest path not tight");
		//				return false;
		//			}
		//		}
		return true;
	}

	// throw an IllegalArgumentException unless {@code 0 <= v < V}
	private void validateVertex( LonLat v ) {
		//		int V = distTo.length;
		if (!g.existeVertice(v))
			throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (-1));
	}

	//	/**
	//	 * Unit tests the {@code DijkstraSP} data type.
	//	 *
	//	 * @param args the command-line arguments
	//	 */
	//	public static void main(String[] args) {
	//		In in = new In(args[0]);
	//		EdgeWeightedDigraph G = new EdgeWeightedDigraph(in);
	//		int s = Integer.parseInt(args[1]);
	//
	//		// compute shortest paths
	//		DijkstraSP sp = new DijkstraSP(G, s);
	//
	//
	//		// print shortest path
	//		for (int t = 0; t < G.V(); t++) {
	//			if (sp.hasPathTo(t)) {
	//				StdOut.printf("%d to %d (%.2f)  ", s, t, sp.distTo(t));
	//				for (DirectedEdge e : sp.pathTo(t)) {
	//					StdOut.print(e + "   ");
	//				}
	//				StdOut.println();
	//			}
	//			else {
	//				StdOut.printf("%d to %d         no path\n", s, t);
	//			}
	//		}
	//	}

}
