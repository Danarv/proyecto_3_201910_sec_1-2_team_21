package model.data_structures;

public interface IHashTable<K, V> {
	
	/**
	 * Agrega un entrada a la tabla con el elemento y llave especificados. En caso que ya exista un elemento con esa llave, ser� reemplazado.
	 * @param llave La llave asociada con el elemento.
	 * @param elemento El elemento a ser agregado.
	 * @return El antiguo elemento asociado con la llave o <code>null</code> si no ten�a ning�n elemento asociado.
	 */
	public V agregar( K llave, V elemento );

	/**
	* Retorna el elemento asociado con la llave especificada.
	* @param llave La llave del elemento que se desea recuperar.
	* @return El elemento asociado con la llave o <code>null</code> si no existe tal asociaci�n.
	*/
	public V dar( K llave );

	/**
	* Elimina el elemento asociado con la llave especificada.
	* @param llave Llave del elemento que se desea eliminar.
	* @return El elemento eliminado o <code>null</code> si la llave no tiene ning�n elemento asociado en la tabla.
	*/
	public V eliminar( K llave );

	/**
	* Retorna el n�mero de elementos que hay en la tabla
	* @return El n�mero de elementos que hay en la tabla
	*/
	public int darNumeroElementos( );
}
