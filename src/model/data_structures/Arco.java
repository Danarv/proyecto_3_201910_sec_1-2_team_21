package model.data_structures;

/**
* Representa un arco del grafo
* @param <K> Tipo del identificador de un v�rtice
* @param <V> Tipo de datos del elemento del v�rtice
* @param <A> Tipo de datos del elemento del arco
*/
public class Arco <K, V, A extends IArco> implements IArco {
	
	//Atributos
	
	/**
	 * V�rtice desde el cual sale el arco
	 */
	private Vertice<K, V, A> origen;

	/**
	 * V�rtice hacia el cual va el arco
	 */
	private Vertice<K, V, A> destino;

	/**
	 * Elemento en el arco
	 */
	private A infoArco;
	
	//Constructor
	
	/**
	 * Constructor del arco
	 * @param pExtremo1 V�rtice desde el cual sale el arco
	 * @param pExtremo2 V�rtice hacia donde se dirige el arco
	 * @param pInfoArco Elemento en el arco
	 */
	public Arco(Vertice<K, V, A> pOrigen, Vertice<K, V, A> pDestino, A pInfoArco) {
		origen = pOrigen;
		destino = pDestino;
		infoArco = pInfoArco;
	}
	
	//Metodos
	
	/**
	 * Devuelve la informacion del arco
	 * @return info arco
	 */
	public A getInfoArco() {
		return infoArco;
	}
	
	/**
	 * Cambia la informacion del arco
	 * @param pNewInfo nueva informacion del arco
	 */
	public void cambiarInfo(A pNewInfo) {
		infoArco = pNewInfo;
	}
	

	/**
	 * Devuelve el v�rtice origen del arci
	 * @return v�rtice origen del arco
	 */
	public Vertice<K, V, A> getVerticeOrigen() {
		return origen;
	}
	
	/**
	 * Devuelve el v�rtice destino del arco
	 * @return v�rtice destino del arco
	 */
	public Vertice<K, V, A> getVerticeDestino() {
		return destino;
	}
	
	/**
	 * Devuelve el peso del arco
	 * @return Peso del arco
	 */
	public double darPeso() {
		return infoArco.darPeso();
	}
}
