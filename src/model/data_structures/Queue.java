package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T>  {

	
	class Node <T>
	{
		private T item;
		
		private Node<T> siguiente;
		
		public Node(T pItem)
		{
			this.item = pItem;
			
			this.siguiente = null;
		}
		
		public T getItem() {
			return item;
		}
		
		public Node<T> getNext() {
			return siguiente;
		}
		
		public Node<T> desconectarPrimero(){
			Node<T> x = siguiente;
			siguiente = null;
			return x;
		}
		
		public Node<T> setNext(Node<T> pNodo) {
			siguiente = pNodo;
			return pNodo;
		}
	}
	
	//--------------------------------------------------------------------------------------
	// Atributos
	//--------------------------------------------------------------------------------------
	
	/**
	 * Primer elemento de la cola
	 */
	private Node<T> first;
	
	/**
	 * �ltimo elemento de la cola
	 */
	private Node<T> last;
	
	/**
	 * Tama�o de la cola
	 */
	private int size;
	
	//--------------------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------------------
	
	/**
	 * Crea una nueva cola vac�a
	 */
	public Queue() {
		first = null;
		last = null;
		size = 0;
	}
	
	//--------------------------------------------------------------------------------------
	// M�todos
	//--------------------------------------------------------------------------------------
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}
	
	/**
	 * Clase que representa el iterador para la cola
	 */
	private class ListIterator implements Iterator<T> {
		private Node<T> current = first;
		
		public boolean hasNext() {
		 return current != null; 
		}
		
		public T next() {
			 T item = current.item;
			 current = current.siguiente;
			 return item;
		}
	 }

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first == null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	public void enqueue(T t) {
		// TODO Auto-generated method stub
		Node<T> nuevo = new Node<T>(t);
		
		if(isEmpty()){
			first= nuevo;
			last = nuevo;
		}
		else {
			last = last.setNext(nuevo);
		}
		
		size++;
	}

	@Override
	public T dequeue() throws Exception{
		// TODO Auto-generated method stub
		
		if(isEmpty()) {
			throw new Exception("La cola est� vac�a");
		}
		else {
			Node<T> get = first;
			first = first.desconectarPrimero();
			
			if(first == null) last = null;
			size--;
			return get.getItem();
		}
	}
	
	public Node<T> getNodoFirst() { 
		return first;
	}
	
	public Node<T> getNodoLast() {
		return last;
	}
}
