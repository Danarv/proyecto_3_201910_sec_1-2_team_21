package model.data_structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import model.exceptions.ArcoNoExisteException;
import model.exceptions.ArcoYaExisteException;
import model.exceptions.VerticeNoExisteException;
import model.exceptions.VerticeYaExisteException;
import model.vo.InfoArco;

/**
* Representa un grafo dirigido
* 
* @param <K> Tipo del identificador de un v�rtice
* @param <V> Tipo de datos del elemento del v�rtice
* @param <InfoArco> Tipo de datos del elemento del arco
*/
public class DiGraph <K, V, A extends IArco> {
	
	//Atributos
	
	/**
	 * Tabla hash con los vertices del grafo
	 */
	private Hashtable<K, Vertice<K, V, A>> vertices;
	
	/**
	 * Cantidad de vertices
	 */
	private int nVertices;
	
	/**
	 * Cantidad de arcos
	 */
	private int nArcos;
	
	//Constructor
	public DiGraph() {
		this.nVertices = 0;
		this.nArcos = 0;
		vertices = new Hashtable<K, Vertice<K, V, A>>();
	}

	//M�todos
	
	/**
	 * Devuelve el n�mero de vertices en el grafo
	 * @return int n�mero de vertices
	 */
	public int getNVertex() {
		return nVertices;
	}
	
	/**
	 * Devuelve el n�mero de arcos grafo
	 * @return int npumero de arcos
	 */
	public int getNEdges() {
		return nArcos;
	}
	
	/**
	 * Retorna los v�rtices del grafo.
	 * @return Los v�rtices del grafo.
	 */
	public ArrayList<Vertice<K, V, A>> darVertices() {
		// Crear la lista
		ArrayList<V> vs = new ArrayList<V>();

		// Recorrer los v�rtices y poblar la lista
		for(Vertice<K, V, A> v : vertices.values()) {
			vs.add(v.getInfo());
		}

		// Retornar la lista
		return vs;
	}
	
	/**
	 * Retorna el arco entre los v�rtices ingresados por parametros
	 * @param idVertexIni id del vertice inicial del arco
	 * @param idVertexFin id del vertice final del arco
	 * @return El arco entre los v�rtices ingresados por parametros
	 * @throws VerticeNoExisteException si alguno de los v�rtices ingresados por parametros no existe en el grafo
	 * @throws ArcoNoExisteException si no existe un arco entre esos v�rtices
	*/
	public Arco<K, V, A> darArco(K idVertexIni, K idVertexFin) throws VerticeNoExisteException, ArcoNoExisteException {
		// Busca el primer v�rtice y luego busca el arco
		Vertice<K, V, A> vertice1 = vertices.get(idVertexIni);
		if(vertices.get(idVertexFin) != null) {
			Arco<K, V, A> arco = vertice1.getArco(idVertexFin);
			if( arco == null ) {
				throw new ArcoNoExisteException( "No existe un arco entre los v�rtices seleccionados", idVertexIni, idVertexFin );
			}
			else {
				return arco;
			}
		}
		else
		throw new VerticeNoExisteException( "V�rtice destino no existe", idVertexFin);
	}
	
	/**
	 * Agrega un vertice con un id �nico
	 * @param idVertex id �nico del vertice
	 * @param infoVertexinformaci�n del vertice
	 */
	public void addVertex(K idVertex, V infoVertex) throws VerticeYaExisteException{
		
		if(vertices.get(idVertex) != null) {
			throw new VerticeYaExisteException("El vertice ya existe", idVertex);
		}
		else {
			Vertice<K, V, A> x = new Vertice<K, V, A>(infoVertex, idVertex);
			vertices.put(idVertex, x);
			nVertices++;
		}
	}
	
	/**
	 * Agrega un arco entre dos vertices
	 * @param idVertexIni id del vertice inicial del arco
	 * @param idVertexFin id del vertice final del arco
	 * @param infoArc informaci�n del arco
	 * @throws ArcoYaExisteException 
	 */
	public void addEdge(K idVertexIni, K idVertexFin, A infoArc) throws VerticeNoExisteException, ArcoYaExisteException {
		Vertice<K, V, A> extremo1 = vertices.get(idVertexIni);
		Vertice<K, V, A> extremo2 = vertices.get(idVertexFin);
		
		Arco<K, V, A> arco = new Arco<K, V, A>( extremo1, extremo2, infoArc );
		
		extremo1.agregarArco( arco );
		nArcos++;
	}
	
	/**
	 * Elimina un vertice del grafo
	 * @param idVertex id del vertice a eliminar
	 */
	public void deleteVertex(K idVertex) throws VerticeNoExisteException{
		Vertice<K, V, A> x = vertices.get(idVertex);
		
		for(Vertice<K, V, A> vert : vertices.values()) {
			try {
				vert.eliminarArco(x.getId());
			}
			catch(ArcoNoExisteException e) {
				
			}
		}
		
		vertices.remove(idVertex);
		nVertices--;
	}
	
	/**
	 * Elimina un arco
	 * @param idVertex1 id del primer vertice del arco
	 * @param idVertex2 id del segundo vertice del arco
	 */
	public void deleteEdge(K idVertex1,K idVertex2) throws VerticeNoExisteException, ArcoNoExisteException{
		Vertice<K, V, A> extremo1 = vertices.get(idVertex1);
		extremo1.eliminarArco(idVertex2);
		nArcos--;
	}
	
	/**
	 * Obtiene la informaci�n de un vertice
	 * @param idVertex id del vertice
	 * @return V informaci�n
	 */
	public V getInfoVertex(K idVertex) {
		Vertice<K, V, A> x = vertices.get(idVertex);
		if(x != null) {
			return x.getInfo();
		}
		return null;
	}

	/**
	 * Modifica la informacion de un vertice
	 * @param idVertex id del vertice a modificar
	 * @param infoVertex nueva informaci�n del vertice
	 */
	public void setInfoVertex(K idVertex, V infoVertex) {
		Vertice<K, V, A> x = (Vertice<K, V, A>) vertices.get(idVertex);
		
		while(x != null) {
			x.cambiarInfoVertice(infoVertex);
		}
	}

	/**
	 * Obtiene la informaci�n de un arco
	 * @param idVertexIni id del vertice inicial del arco
	 * @param idVertexFin id del vertice final del arco
	 * @return
	 */
	public A getInfoArc(K idVertexIni, K idVertexFin) {
		A info = null;
		try {
			Arco<K, V, A> x = darArco(idVertexIni, idVertexFin);
			info = x.getInfoArco();
		} catch (VerticeNoExisteException | ArcoNoExisteException e) {
			e.printStackTrace();
		}
		
		return info;
	}

	/**
	 * Modifica la informacion de un arco
	 * @param idVertexIni id del vertice inicial del arco
	 * @param idVertexFin id del vertice final del arco
	 * @param infoArc nueva informaci�n del arco
	 */
	public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc) {
		Arco<K, V, A> x = vertices.get(idVertexIni).getArco(idVertexFin);
		x.cambiarInfo(infoArc);
	}
	
	/**
	 * Retorna los identificadores de los v�rtices adyacentes a un vertice
	 * @param idVertex id del vertice referencia
	 * @return
	 */
	public Iterator<K> adj(K idVertex) {
		Vertice<K, V, A> x = (Vertice<K, V, A>) vertices.get(idVertex);
		
		while(x != null) {
			//return (Iterator<K>) x.getListaAdyacencia().iterator();
		}
		return null;
	}
}
