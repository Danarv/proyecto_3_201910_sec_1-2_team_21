package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import model.exceptions.ArcoNoExisteException;
import model.exceptions.ArcoYaExisteException;
import model.vo.InfoArco;

/**
* Representa un v�rtice del grafo
* 
* @param <K> Tipo del identificador de un v�rtice
* @param <V> Tipo de datos del elemento del v�rtice
* @param <InfoArco> Tipo de datos del elemento del arco
*/
public class Vertice<K, V, InfoArco extends IArco> {
	
	//Atributos
	
	/**
	 * Informaci�n que contiene el vertice
	 */
	private V infoVertice;
	
	/**
	 * Lista de vertices sucesores que forman un arco con el actual (Vertices destino)
	 */
	private ArrayList<Arco<K, V, InfoArco>> listaAdyacentesSucesores;
	
	/**
	 * Lista de vertices predecesores que forman un arco con el actual (Vertices origen)
	 */
	private ArrayList<Arco<K, V, InfoArco>> listaAdyacentesPredecesores;
	
	private boolean marcado;
	
	private K id;
	
	//Constructor
	
	public Vertice(V pInfoVertice, K pId) {
		infoVertice = pInfoVertice;
		id = pId;
		listaAdyacentesSucesores = new ArrayList<Arco<K, V, InfoArco>>();
		listaAdyacentesPredecesores = new ArrayList<Arco<K, V, InfoArco>>();
		marcado = false;
	}
	
	//M�todos
	
	/**
	 * Retorna el id del vertice
	 */
	public K getId() {
		return id;
	}
	
	/**
	 * Retorna la informaci�n del vertice
	 * @return info vertex
	 */
	public V getInfo() {
		return infoVertice;
	}
	
	/**
	 * Retorna la marca del vertice
	 * @return true si est� marcado, false si no
	 */
	public boolean estaMarcado() {
		return marcado;
	}
	
	/**
	 * Retorna la lista de adyacencia sucesores del actual vertice
	 * @return lista de adyacencia
	 */
	public ArrayList<Arco<K, V, InfoArco>> getListaAdyacentesSucesores() {
		return listaAdyacentesSucesores;
	}
	
	/**
	 * Retorna la lista de adyacencia predecesores del actual vertice
	 * @return lista de adyacencia
	 */
	public ArrayList<Arco<K, V, InfoArco>> getListaAdyacentesPredecesores() {
		return listaAdyacentesPredecesores;
	}
	
	/**
	 * Cambia la informaci�n del vertice actual
	 * @param pNewInfo nueva info del vertice
	 */
	public void cambiarInfoVertice(V pNewInfo) {
		infoVertice = pNewInfo;
	}
	
	/**
	 * Marca como visitado el vertice
	 */
	public void marcar() {
		this.marcado = true;
	}
	
	/**
	 * Desmarca como visitado el vertice
	 */
	public void desmarcar() {
		this.marcado = false;
	}
	
	/**
	 * Devuelve el arco (si existe) hacia el v�rtice especificado. Devuelve null si no existe.
	 * @param idVerticeDestino Identificador del v�rtice
	 * @return Arco hacia el v�rtice especificado, null si no existe
	 */
	public Arco<K, V, InfoArco> getArco(K idVerticeDestino) {
		for(int i = 0; i < listaAdyacentesSucesores.size(); i++) {
			Arco<K, V, InfoArco> arco = listaAdyacentesSucesores.get(i);
			
			if(arco.getVerticeDestino().getId().equals(idVerticeDestino)) return arco;
		}
		
		return null;
	}
	
	/**
	 * Agrega un arco al v�rtice
	 * @param pNewArco Arco a agregar al v�rtice
	 * @throws ArcoYaExisteException Excepci�n generada cuando ya hay un arco hacia el mismo v�rtice
	 */
	public void agregarArco(Arco<K, V, InfoArco> pNewArco) throws ArcoYaExisteException{
		K idDestino = pNewArco.getVerticeDestino().getId();
		
		if( getArco(idDestino) != null ){
			throw new ArcoYaExisteException( "El arco ya existe", getId( ), idDestino );
		}
		else {
			listaAdyacentesSucesores.add(pNewArco);
			pNewArco.getVerticeDestino().agregarArcoPredecesor(pNewArco);
		}
	}
	
	/**
	 * Agrega un arco al v�rtice
	 * @param arco Arco a agregar al v�rtice
	 * @throws ArcoYaExisteException Excepci�n generada cuando ya hay un arco hacia el mismo v�rtice
	 */
	private void agregarArcoPredecesor(Arco<K, V, InfoArco> pNewArco) throws ArcoYaExisteException {
		listaAdyacentesPredecesores.add(pNewArco);
	}

	/**
	* Elimina un arco del v�rtice
	* @param idVerticeDestino Identificador del v�rtice destino del arco que se quiere eliminar
	* @throws ArcoNoExisteException Excepci�n generada cuando el arco no existe
	*/
	public void eliminarArco(K idVerticeDestino) throws ArcoNoExisteException {
		Arco<K, V, InfoArco> arco = getArco( idVerticeDestino );
		
		if( arco == null ){
			throw new ArcoNoExisteException( "El arco no existe", getId( ), idVerticeDestino );
		}
		else {
			listaAdyacentesSucesores.remove(arco);
			arco.getVerticeDestino().eliminarArcoPredecesor(arco);
		}
		
	}
	
	/**
	 * Elimina un arco de los predecesores del v�rtice
	 * @param arco Arco a eliminar
	*/
	private void eliminarArcoPredecesor(Arco<K, V, InfoArco> arco) throws ArcoNoExisteException {
		listaAdyacentesPredecesores.remove(arco);
	}
	
	/**
	 * Elimina todos los arcos del v�rtice
	 */
	public void eliminarArcos( ) {
		listaAdyacentesSucesores.clear( );
	}
}
