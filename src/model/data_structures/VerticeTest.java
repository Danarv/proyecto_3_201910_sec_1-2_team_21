package model.data_structures;

public class VerticeTest implements IVertice<Integer>{
	
	//Atributos
	
	public int id;
	
	/**
	 * Dato del vertice
	 */
	public int valor;
	
	//Contructor
	
	public VerticeTest(int pId, int pInfo) {
		this.id = pId;
		this.valor = pInfo;
	}
	
	//M�todos
	
	public Integer getId() {
		return id;
	}
	
	public Integer getInfo() {
		return valor;
	}
}
