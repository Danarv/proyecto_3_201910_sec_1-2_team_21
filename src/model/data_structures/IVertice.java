package model.data_structures;

/**
 * Interfaz de un vertice en el grafo
 */
public interface IVertice <K>{
	
	/**
	 * Retorna el id del vertice
	 * @return id
	 */
	public K getId();
}
