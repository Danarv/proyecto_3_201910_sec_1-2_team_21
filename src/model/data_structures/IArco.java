package model.data_structures;

/**
 * Interfaz de un arco del grafo
 * @author david
 *
 */
public interface IArco {
	
	/**
	 * Devuelve el peso del arco
	 * @return peso
	 */
	public double darPeso( );
}
