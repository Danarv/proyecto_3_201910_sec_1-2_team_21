package model.logic;

import model.data_structures.Arco;
import model.data_structures.DiGraph;
import model.vo.InfoArco;
import model.vo.InfoVertice;
import model.vo.VOMovingViolations;

public class MovingViolationsManager {
	
	//Constantes
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Enero
	 */
	public static final String DATOS_MES_1 = "./data/January_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Febrero
	 */
	public static final String DATOS_MES_2 = "./data/February_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Marzo
	 */
	public static final String DATOS_MES_3 = "./data/Mach_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Abril
	 */
	public static final String DATOS_MES_4 = "./data/Abril_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Mayo
	 */
	public static final String DATOS_MES_5 = "./data/May_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Junio
	 */
	public static final String DATOS_MES_6= "./data/June_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Julio
	 */
	public static final String DATOS_MES_7 = "./data/July_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Agosto
	 */
	public static final String DATOS_MES_8 = "./data/August_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Septiembre
	 */
	public static final String DATOS_MES_9 = "./data/September_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Octubre
	 */
	public static final String DATOS_MES_10 = "./data/October_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Noviembre
	 */
	public static final String DATOS_MES_11 = "./data/November_wgs84.csv";
	
	/**
	 * Constante que representa los datos de las infracciones realizadas en Diciembre
	 */
	public static final String DATOS_MES_12 = "./data/December_wgs84.csv";
	
	public DiGraph<Integer, InfoVertice, InfoArco> grafo;
	
	public cargarGrafo()
	
}